# Hivemind

Hivemind is a Kotlin library for synchronizing data among multiple data sources.<br>
<br>
main: [![pipeline status](https://gitlab.com/growingstems/hivemind/badges/main/pipeline.svg)](https://gitlab.com/growingstems/hivemind/-/commits/main)<br>
[Docs Here!](https://growingstems.gitlab.io/hivemind/)<br><br>
