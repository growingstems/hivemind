package org.growingstems.hivemind

import kotlinx.serialization.json.JsonObject

interface DatabaseHandlerI {
    fun takeApplyCommand(data: Command): String
    fun dequeCommand(): Command { return commandQueue.first() }
    var commandQueue: ArrayDeque<Command>
}