package org.growingstems.hivemind

import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.Json
import kotlin.collections.ArrayDeque

class SQLiteHandler: DatabaseHandlerI {

    override var commandQueue: ArrayDeque<Command>

    constructor(
        databaseLocation: String = "",
    ) {
        commandQueue = ArrayDeque()
    }

    override fun takeApplyCommand(data: Command): String {
        return ""
    }
}