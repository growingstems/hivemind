package org.growingstems.hivemind

import java.io.IOException
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.*
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.charset.Charset
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong


class TCPServer: Runnable, AutoCloseable, TransferServerI<Command> {

    override var recvQueue: ArrayBlockingQueue<Command>
    override var sendQueue: ArrayBlockingQueue<Command>
    override var numClientConnections: Int = 0
        get() = 0
        private set
    private val recvBufSize: Int
    private val sendBufSize: Int
    private val messageReceptionCount = AtomicLong(0)

    constructor(host: String = "localhost",
                port: Int = 0,
                receiveBufferSize: Int = 32 * 1024,
                sendBufferSize: Int = 32 * 1024,
                messageStoreCapacity: Int = 1024) {

        recvBufSize = receiveBufferSize
        sendBufSize = sendBufferSize
        sendQueue = ArrayBlockingQueue(messageStoreCapacity)
        recvQueue = ArrayBlockingQueue(messageStoreCapacity)

        Thread(this).apply { 
            name = "TCPSERVER-THREAD-${host}-${port}"
            isDaemon = true
            start()
         }
    }

    override fun run() {}

    override fun close() {}

    override fun enqueSend(data: Command) {
        sendQueue.offer(data)
    }

    override fun dequeRecv(): Command {
        return recvQueue.take()
    }

    /* fun getPort(): Int = (serverSocket.localAddress as InetSocketAddress).port

    fun getHost(): String = (serverSocket.localAddress as InetSocketAddress).hostString

    fun takeMessage(timeout: Duration = Duration.ofSeconds(0)): String? = recvMessages.poll(timeout.toMillis(), TimeUnit.MILLISECONDS)

    fun giveMessage(message: String): Boolean {
        var given: Boolean = true
        if (!sendMessages.offer(message))
        {
            given = false
            log("warning: send message storage full, discarding message: ${message}")
        }
        
        return given
    }

    fun messageReceptionCount() = messageReceptionCount.get()

    private fun log(message: String) = println("[${LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS)}] Main: ${message}")

    private fun storeMessage(buffer: ByteBuffer) {
        messageReceptionCount.incrementAndGet()
        val message = UTF_8.decode(buffer).toString()
        if (!recvMessages.offer(message)) {
            log("warning: recieve message storage full, discarding message: ${message}")
        }
    }

    private fun SelectionKey.receive() {
        val channel = channel() as SocketChannel
        val buffer = attachment() as ByteBuffer
        try {
            val read = channel.read(buffer);

            var i = 0
            while (i < buffer.position()) {
                if (buffer.get(i) == END_MESSAGE_MARKER) {
                storeMessage(buffer.duplicate().position(i).flip())
                buffer.limit(buffer.position()).position(i + 1).compact()
                i = 0
                } else i += 1
            }

            if (!buffer.hasRemaining()) {
                log("error: client buffer overflow, too big message, discarding buffer")
                buffer.clear()
              }
        
            if (read == -1) {
                if (buffer.position() > 0) {
                    storeMessage(buffer.flip())
                }
                //cancel()
                //channel.close()
            }
        } catch (e: IOException) {
            log("error reading from client: ${e.javaClass}: ${e.message}")
        }

    }


    private fun SelectionKey.send() {
        val channel = channel() as SocketChannel
        val buffer = ByteBuffer.allocate(sendBufSize)

        try {
            while (!sendMessages.isEmpty())
            {
                val message = sendMessages.poll()
                buffer.put(message.toByteArray(Charsets.UTF_8))

                buffer.flip()
                val sent = channel.write(buffer)
                buffer.clear()
            }
            channel.close()
            
        } catch (e: IOException) {
            log("error sending to client: ${e.javaClass}: ${e.message}")
        }

        //channel.register(selector, SelectionKey.OP_READ).attach(ByteBuffer.allocate(recvBufSize))
    }

    private fun SelectionKey.acceptClient() {
        val channel = (channel() as ServerSocketChannel).accept()
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ).attach(ByteBuffer.allocate(recvBufSize))
    }

    override fun run() {
        while(selector.isOpen) {
            try {
                selector.select { selectionKey ->
                    if (selectionKey.isAcceptable) {
                        selectionKey.acceptClient()
                    }
                    else if (selectionKey.isReadable) {
                        selectionKey.receive()
                        selectionKey.send()
                    }
                }
            } catch (closed: ClosedSelectorException) {
            } catch (e: IOException) {
                log("error: ${e.javaClass.simpleName}: ${e.message}")
            }
        }
        log("close")
    }

    override fun close() {
        selector.close()
        serverSocket.close()
    } */
}