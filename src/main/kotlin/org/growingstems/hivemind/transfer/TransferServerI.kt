package org.growingstems.hivemind

import java.util.concurrent.ArrayBlockingQueue

interface TransferServerI<T> {
    var sendQueue: ArrayBlockingQueue<T>
    var recvQueue: ArrayBlockingQueue<T>
    val numClientConnections: Int
    fun enqueSend(data: T)
    fun dequeRecv(): T
}
