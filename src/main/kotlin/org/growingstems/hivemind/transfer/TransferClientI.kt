package org.growingstems.hivemind

import java.util.concurrent.ArrayBlockingQueue

interface TransferClientI<T> {
    var sendQueue: ArrayBlockingQueue<T>
    var recvQueue: ArrayBlockingQueue<T>
    var hasConnection: Boolean
    fun enqueSend(data: T)
    fun dequeRecv(): T
}