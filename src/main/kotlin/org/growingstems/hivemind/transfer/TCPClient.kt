package org.growingstems.hivemind

import java.util.concurrent.ArrayBlockingQueue

class TCPClient: Runnable, AutoCloseable, TransferClientI<Command> {

    override var recvQueue: ArrayBlockingQueue<Command>
    override var sendQueue: ArrayBlockingQueue<Command>
    override var hasConnection: Boolean
    private val recvBufSize: Int
    private val sendBufSize: Int

    constructor(host: String = "localhost",
                port: Int = 0,
                receiveBufferSize: Int = 32 * 1024,
                sendBufferSize: Int = 32 * 1024,
                messageStoreCapacity: Int = 1024) {

        recvBufSize = receiveBufferSize
        sendBufSize = sendBufferSize
        hasConnection = false
        sendQueue = ArrayBlockingQueue(messageStoreCapacity)
        recvQueue = ArrayBlockingQueue(messageStoreCapacity)
    }

    override fun run () {}

    override fun close() {}

    override fun enqueSend(data: Command) {
        sendQueue.offer(data)
    }

    override fun dequeRecv(): Command {
        return recvQueue.take()
    }
}