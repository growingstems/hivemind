package org.growingstems.hivemind

import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import java.util.UUID
import java.util.Date
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString
import kotlinx.serialization.decodeFromString

private fun log(message: String) = println("[${LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS)}] Main: ${message}")

fun main(args: Array<String>) {
    log("Hivemind")

    

    /*
    val date = Date()
    val cmd = Command(
        opcode = CommandOpcode.CREATE,
        timestamp = date,
        rowId = UUID.fromString("48a3ec7f-af59-4fb8-9268-3baf391b7ec7"),
        table = "Test",
        rowData = arrayListOf<Pair<String, String>>(Pair("Column", "Data"), Pair("Column2", "Data2"))
    )

    val jsonString: String = Json.encodeToString(cmd)

    log(jsonString)

    val decodedCmd: Command = Json.decodeFromString<Command>(jsonString)

    log(decodedCmd.opcode.toString())
    log(decodedCmd.timestamp.toString())
    log(decodedCmd.rowId.toString())
    log(decodedCmd.table.toString())
    log(decodedCmd.rowData.toString())
    */
}