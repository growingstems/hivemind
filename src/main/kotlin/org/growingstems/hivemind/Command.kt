package org.growingstems.hivemind

import java.util.Date
import java.util.UUID
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * The CommandOpcode enum class stores the possible values of Command.opcode. 
 * [CommandOpcode.from by JB Nizet](https://stackoverflow.com/questions/37794850/effective-enums-in-kotlin-with-reverse-lookup/37795810#37795810).
 * 
 * @param value the Byte value of the opcode (0, 1, 2)
 */
enum class CommandOpcode {
    /** denotes a database entry creation operation */
    CREATE,
    /** denotes a database entry update operation */
    UPDATE,
    /** denotes a database entry deletion operation */
    DELETE;
}

/**
 * The DateSerializer class provides a serialization template for the java.util.Date class
 */
@Serializer(forClass = Date::class)
@OptIn(ExperimentalSerializationApi::class)
object DateSerializer : KSerializer<Date> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Date", PrimitiveKind.LONG)
    override fun serialize(encoder: Encoder, value: Date) = encoder.encodeLong(value.getTime())
    override fun deserialize(decoder: Decoder) = Date(decoder.decodeLong())
}

/**
 * The UUIDSerializer class provides a serialization template for the java.util.UUID class
 */
@Serializer(forClass = UUID::class)
@OptIn(ExperimentalSerializationApi::class)
object UUIDSerializer : KSerializer<UUID> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("UUID", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: UUID) = encoder.encodeString(value.toString())
    override fun deserialize(decoder: Decoder) = UUID.fromString(decoder.decodeString())
}

/**
 * The Command data class provides a template for sending a generalized data command
 * between instances of Hivemind.
 * 
 * @property opcode the action to be taken by the recieving DAO implementation, one of either Create (0), Update (1), or Delete (2)
 * @property timestamp syncronized time for record, optional: defaults to time of instantiation if not specified
 * @property rowId the UUID of the row to be acted on
 * @property table the table to be acted on
 * @property rowData the column, data of the given row to be acted on
 */
@Serializable
data class Command (
    val opcode: CommandOpcode,
    @OptIn(ExperimentalSerializationApi::class)
    @EncodeDefault
    @Serializable(with=DateSerializer::class) val timestamp: Date = Date(),
    @Serializable(with=UUIDSerializer::class) val rowId: UUID,
    val table: String,
    val rowData: ArrayList<Pair<String, String>>
)