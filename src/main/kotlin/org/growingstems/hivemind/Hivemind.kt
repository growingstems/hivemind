package org.growingstems.hivemind

import org.growingstems.hivemind.TCPServer
import org.growingstems.hivemind.TCPClient
import org.growingstems.hivemind.SQLiteHandler

enum class TransferProto { TCP, TCPASYNC, BLUETOOTH } // Default TCP
enum class PersistProto { SQLITE, MYSQL } // Default SQLITE

class Hivemind: Runnable {
    private val nodeId: String

    private val transferServer: TransferServerI<Command>
    private val transferClient: TransferClientI<Command>

    private val persistDB: DatabaseHandlerI

    constructor(
        id: String,
        serverType: TransferProto,
        clientType: TransferProto,
        persistDBType: PersistProto,
    ) {
        nodeId = id

        transferServer = when(serverType) {
            TransferProto.TCP -> TCPServer()
            TransferProto.TCPASYNC -> TCPServer()
            TransferProto.BLUETOOTH -> TCPServer()
        }

        transferClient = when(clientType) {
            TransferProto.TCP -> TCPClient()
            TransferProto.TCPASYNC -> TCPClient()
            TransferProto.BLUETOOTH -> TCPClient()
        }

        persistDB = when(persistDBType) {
            PersistProto.SQLITE -> SQLiteHandler()
            PersistProto.MYSQL -> SQLiteHandler()
        }
    }

    /**
     * If the transferClient does not have a connection it is assumed to be the root node
     */
    fun isRoot(): Boolean {
        return !transferClient.hasConnection
    }

    override fun run() {
        if (isRoot()) {
            val cmd = transferServer.dequeRecv()
            persistDB.takeApplyCommand(cmd)
            transferServer.enqueSend(cmd)
        } else {
            var cmd = transferServer.dequeRecv()
            transferClient.enqueSend(cmd)
            cmd = transferClient.dequeRecv()
            persistDB.takeApplyCommand(cmd)
            transferServer.enqueSend(cmd)
        }
    }
}