package org.growingstems.hivemind

import org.growingstems.hivemind.Command

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import java.util.UUID
import java.util.Date
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.SerializationException

class CommandTest {
    val date = Date()

    @Test fun initWithCurrentDate() {
        val cmd = Command(opcode = CommandOpcode.CREATE, timestamp = date, rowId = UUID.fromString("39013d4c-3059-11ed-a261-0242ac120002"), table = "Test", rowData = arrayListOf<Pair<String, String>>(Pair("Column", "Data")))

        assertEquals(date, cmd.timestamp)
    }

    @Test fun encodeCommandOpcode() {
        var cmdString: String =  """{"opcode":"CREATE","timestamp":0,"rowId":"39013d4c-3059-11ed-a261-0242ac120002","table":"Test","rowData":[{"first":"Column","second":"Data"}]}"""
        var testCmd: Command = Json.decodeFromString(cmdString)
        assertEquals(CommandOpcode.CREATE, testCmd.opcode)

        cmdString =  """{"opcode":"UPDATE","timestamp":0,"rowId":"39013d4c-3059-11ed-a261-0242ac120002","table":"Test","rowData":[{"first":"Column","second":"Data"}]}"""
        testCmd = Json.decodeFromString(cmdString)
        assertEquals(CommandOpcode.UPDATE, testCmd.opcode)

        cmdString =  """{"opcode":"DELETE","timestamp":0,"rowId":"39013d4c-3059-11ed-a261-0242ac120002","table":"Test","rowData":[{"first":"Column","second":"Data"}]}"""
        testCmd = Json.decodeFromString(cmdString)
        assertEquals(CommandOpcode.DELETE, testCmd.opcode)

    }

    @Test fun commandToJsonStringWithCurrentDate() {
        val cmd = Command(opcode = CommandOpcode.CREATE, timestamp = date, rowId = UUID.fromString("39013d4c-3059-11ed-a261-0242ac120002"), table = "Test", rowData = arrayListOf<Pair<String, String>>(Pair("Column", "Data")))

        val jsonString: String = Json.encodeToString(cmd)

        assertEquals("""{"opcode":"CREATE","timestamp":""" + date.time.toString() + ""","rowId":"39013d4c-3059-11ed-a261-0242ac120002","table":"Test","rowData":[{"first":"Column","second":"Data"}]}""", jsonString)
    }

    @Test fun jsonStringToCommandWithCurrentDate() {
        val cmdString: String =  """{"opcode":"CREATE","timestamp":""" + date.time.toString() + ""","rowId":"39013d4c-3059-11ed-a261-0242ac120002","table":"Test","rowData":[{"first":"Column","second":"Data"}]}"""
        
        val testCmd = Json.decodeFromString<Command>(cmdString)

        assertEquals(CommandOpcode.CREATE, testCmd.opcode)
        assertEquals(date.time, testCmd.timestamp.time)
        assertEquals(UUID.fromString("39013d4c-3059-11ed-a261-0242ac120002"), testCmd.rowId)
        assertEquals("Test", testCmd.table)
        assertEquals(arrayListOf<Pair<String, String>>(Pair("Column", "Data")).toList(), testCmd.rowData.toList())
    }

    @Test fun commandToJsonStringWithMultipleRowData() {
        val cmd = Command(
            opcode = CommandOpcode.CREATE,
            timestamp = date,
            rowId = UUID.fromString("48a3ec7f-af59-4fb8-9268-3baf391b7ec7"),
            table = "Test",
            rowData = arrayListOf<Pair<String, String>>(Pair("Column", "Data"), Pair("Column2", "Data2"))
        )

        val jsonString: String = Json.encodeToString(cmd)
        val compareString: String =
            """{"opcode":"CREATE","timestamp":""" + date.time.toString() + ""","rowId":"48a3ec7f-af59-4fb8-9268-3baf391b7ec7","table":"Test","rowData":[{"first":"Column","second":"Data"},{"first":"Column2","second":"Data2"}]}"""

        assertEquals(compareString, jsonString)
    }

    @Test fun encodeDecodeWithSpecialCharactersInText() {
        val cmd = Command(
            opcode = CommandOpcode.UPDATE,
            timestamp = date,
            rowId = UUID.fromString("6b056a22-8ebe-44a2-a948-7ea470163820"),
            table = "TestSpecial",
            rowData = arrayListOf<Pair<String, String>>(Pair(",./;'[]\\-=`~!@#$%^&*()_+{}|:\"<>?)", "Value"), Pair("Column", ",./;'[]\\-=`~!@#$%^&*()_+{}|:\"<>?)"))
        )

        val encodedData: String = Json.encodeToString(cmd)
        val decodedCmd: Command = Json.decodeFromString(encodedData)

        assertEquals(CommandOpcode.UPDATE, decodedCmd.opcode)
        assertEquals(date.time, decodedCmd.timestamp.time)
        assertEquals(UUID.fromString("6b056a22-8ebe-44a2-a948-7ea470163820"), decodedCmd.rowId)
        assertEquals("TestSpecial", decodedCmd.table)
        assertEquals(arrayListOf<Pair<String, String>>(Pair(",./;'[]\\-=`~!@#$%^&*()_+{}|:\"<>?)", "Value"), Pair("Column", ",./;'[]\\-=`~!@#$%^&*()_+{}|:\"<>?)")).toList(), decodedCmd.rowData.toList())
    }

    @Test fun decodeProperCommandOutOfOrder() {
        val cmdString: String = """{"timestamp":""" + date.time.toString() + ""","table":"Test","rowId":"48a3ec7f-af59-4fb8-9268-3baf391b7ec7","rowData":[{"first":"Column","second":"Data"}],"opcode":"CREATE"}"""
        val testCmd: Command = Json.decodeFromString(cmdString)

        assertEquals(CommandOpcode.CREATE, testCmd.opcode)
        assertEquals(date.time, testCmd.timestamp.time)
        assertEquals(UUID.fromString("48a3ec7f-af59-4fb8-9268-3baf391b7ec7"), testCmd.rowId)
        assertEquals("Test", testCmd.table)
        assertEquals(arrayListOf<Pair<String, String>>(Pair("Column", "Data")).toList(), testCmd.rowData.toList())
    }

    @Test fun decodeWithImproperCommandFails() {
        val cmdString: String = """{"opcode":"CREAT","timestamp":""" + date.time.toString() + ""","rowId":"48a3ec7f-af59-4fb8-9268-3baf391b7ec7","table":"Test","rowData":[{"first":"Column","second":"Data"},{"first":"Column2","second":"Data2"}]}"""
        assertThrows(SerializationException::class.java) {
            Json.decodeFromString(cmdString)
        }
    }

    @Test fun decodeWithImproperDateFails() {
        val cmdString: String = """{"opcode":"CREATE","timestamp":"1","rowId":"48a3ec7f-af59-4fb8-9268-3baf391b7ec7","table":"Test","rowData":[{"first":"Column","second":"Data"},{"first":"Column2","second":"Data2"}]}"""
        assertThrows(SerializationException::class.java) {
            Json.decodeFromString(cmdString)
        }
    }

    @Test fun decodeWithImproperColumnDataFails() {
        val cmdString: String = """{"opcode":"CREATE","timestamp":""" + date.time.toString() + ""","rowId":"48a3ec7f-af59-4fb8-9268-3baf391b7ec7","table":"Test","rowData":[{"first":"Column","first":"Data"}]}"""
        assertThrows(SerializationException::class.java) {
            Json.decodeFromString(cmdString)
        }
    }

    @Test fun decodeWithImproperUUIDFails() {
        val cmdString: String = """{"opcode":"CREATE","timestamp":""" + date.time.toString() + ""","rowId":"0","table":"Test","rowData":[{"first":"Column","second":"Data"},{"first":"Column2","second":"Data2"}]}"""
        assertThrows(SerializationException::class.java) {
            Json.decodeFromString(cmdString)
        }
    }
}